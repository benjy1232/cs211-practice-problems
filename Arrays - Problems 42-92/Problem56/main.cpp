//Rotates a matrix 90 degrees
//Only works on square matrices
#include<iostream>
using namespace std;

int ** matrix_creation(int row_size, int col_size){
    int ** matrix = new int*[row_size];
    for(int i = 0; i<row_size; i++){
        matrix[i] = new int[col_size];
        for(int j = 0; j < row_size; j++){
            matrix[i][j] = 0;
        }
    }
    return matrix;
}

void matrix_clear(int ** matrix, int row_size){
    for(int i = 0; i < row_size; i++){
        delete[] matrix[i];
    }
    delete[] matrix;
}

int main(void){
    int row_size, col_size;
    int temp_row, temp_col;

    cout << "Please enter dimensions of matrix: \n";
    cout << "Please enter the number of rows: ";
    cin >> row_size;
    cout << "Please enter the number of columns: ";
    cin >> col_size;

    //Creation of the two matrices
    int ** matrix = matrix_creation(row_size, col_size);
    int ** new_matrix = matrix_creation(col_size, row_size);

    temp_col = col_size - 1;
    temp_row = row_size - 1;
    /*
    [[a b c],
    [d e f],
    [g h i]]
    Turns into 
    [[g d a],
    [h e b],
    [i f c]]
    */
   cout << "Please enter elements for matrix: \n";

   for(int i = 0; i<row_size; i++){
       for(int j = 0; j<col_size; j++){
           cin >> matrix[i][j];
       }
   }

   cout << endl;

   for(int a = 0; a < row_size; a++){
       for(int b = 0; b < col_size; b++){
           new_matrix[a][b] = matrix[temp_col - b][a];
           cout << new_matrix[a][b] << " ";
       }
       cout << endl;
   }

   cout << endl;
   
   matrix_clear(matrix, row_size);
   matrix_clear(new_matrix, col_size);
}